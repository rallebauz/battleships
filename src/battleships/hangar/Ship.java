package battleships.hangar;

import battleships.util.Art;
import javafx.scene.image.Image;

public enum Ship {
	SUBMARINE(1, Art.SUBMARINE),
	DESTROYER(2, Art.DESTROYER),
	CRUISER(3, Art.CRUISER),
	BATTLESHIP(4, Art.BATTLESHIP),
	AIRCRAFT_CARRIER(5, Art.AIRCRAFT_CARRIER);
	
	public static final int NUM_SHIPS;
	private static final Ship[] ships = values();
	
	private final int shipSize;
	private final Image image;
	
	static {
		NUM_SHIPS = ships.length;
	}
	
	Ship(int shipSize, Image image) {
		this.shipSize = shipSize;
		this.image = image;
	}
	
	public Ship next() {
		return ships[(this.ordinal() + 1) % ships.length];
	}
	public int getSize() {
		return shipSize;
	}
	public final Image getImage() {
		return image;
	}
	
	public static Ship first() {
		return ships[0];
	}
}
