package battleships.hangar;

import java.util.EnumMap;
import java.util.Map;

public class Hangar {
	public enum HangarType {
		CLASSIC(2, 2, 1, 1, 1),
		RUSSIAN(4, 3, 2, 1, 0),
		JAPANESE(3, 0, 0, 0, 0);
		
		private final EnumMap<Ship, Integer> shipAmount = new EnumMap<Ship, Integer>(Ship.class);
		
		HangarType(int... numShipType) {
			// Required to check that arguments match what is excepted since constructor accepts varargs.
			checkValidArguments(numShipType);
			Ship[] ships = Ship.values();
			for (int i = 0; i < numShipType.length; i++) {
				shipAmount.put(ships[i], numShipType[i]);
			}
		}
		
		public EnumMap<Ship, Integer> getShipAmount() {
			EnumMap<Ship, Integer> ret = new EnumMap<Ship, Integer>(Ship.class);
			for (Map.Entry<Ship, Integer> entry : shipAmount.entrySet()) {
				ret.put(entry.getKey(), entry.getValue());
			}
			return ret;
		}
		
		private void checkValidArguments(int[] argNumShipType) {
			int numParams = argNumShipType.length;
			if (numParams != Ship.NUM_SHIPS) {
				throw new IllegalArgumentException("Invalid parameter amount (Expected " + Ship.NUM_SHIPS + " but got " + numParams + ")");
			}
			for (int i = 0; i < numParams; i++) {
				if (argNumShipType[i] < 0) {
					throw new IllegalArgumentException("Ship amount can not be less than zero");
				}
			}
		}
	}
	
	private final HangarType hangarType;
	private final EnumMap<Ship, Integer> shipAmount;
	
	public Hangar(HangarType hangarType) {
		this.hangarType = hangarType;
		shipAmount = hangarType.getShipAmount();
	}
	
	public void subtract(Ship ship) {
		int curr = shipAmount.get(ship);
		if (curr <= 0) return;
		shipAmount.put(ship, curr - 1);
	}
	
	public Ship getFirstAvailableShip() {
		for (Map.Entry<Ship, Integer> entry : shipAmount.entrySet()) {
			if (entry.getValue() > 0) return entry.getKey();
		}
		return null;
	}
	public Ship getFirstAvailableShipFrom(Ship ship) {
		if (shipAmount.get(ship) > 0) return ship;
		Ship nextShip = ship.next();
		while (nextShip != ship) {
			if (shipAmount.get(nextShip) > 0) {
				return nextShip;
			} else {
				nextShip = nextShip.next();
			}
		}
		return null;
	}
	public HangarType getHangarType() {
		return hangarType;
	}
	public int getShipAmount(Ship ship) {
		return shipAmount.get(ship);
	}
	public int getNumCurrentShips() {
		int numCurrentShips = 0;
		for (Map.Entry<Ship, Integer> entry : shipAmount.entrySet()) {
			numCurrentShips += entry.getValue();
		}
		return numCurrentShips;
	}
	
	public boolean isEmpty() {
		return (getNumCurrentShips() <= 0);
	}
	public boolean hasAny(Ship ship) {
		return (getShipAmount(ship) > 0);
	}
	public boolean hasNo(Ship ship) {
		return (getShipAmount(ship) <= 0);
	}
}
