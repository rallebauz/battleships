package battleships.board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import battleships.board.Tile.TileType;
import battleships.board.TileBlock.TileBlockFactory;
import battleships.hangar.Hangar;
import battleships.hangar.Ship;
import battleships.util.BattleshipsLogger;
import battleships.util.Orientation;
import battleships.util.Point;
import battleships.util.Utility;

public class BuildBoard extends Board {
	public static final class PlacementStrategy {
		private PlacementStrategy() {}
		
		public static void placeRemainingShipsRandomly(BuildBoard targetBoard) {
			BoardSize targetBoardSize = targetBoard.getBoardSize();
			Ship placementShip;
			while ((placementShip = targetBoard.getHangar().getFirstAvailableShip()) != null) {
				Point coords = Utility.getRandomPointFromZero(targetBoardSize.getNumColumns(), targetBoardSize.getNumRows());
				Orientation orientation = ((Math.random() > 0.5) ? Orientation.HORIZONTAL : Orientation.VERTICAL);
				if (targetBoard.canPlace(placementShip, coords, orientation)) {
					targetBoard.placeShip(placementShip, coords, orientation);
				}
			}
		}
	}
	
	public interface BuildBoardEvent {
		public void onShipPlaced(Ship ship, TileBlock tileBlock);
		public void onAllShipsPlaced();
	}
	
	private final ArrayList<BuildBoardEvent> buildBoardEvents = new ArrayList<BuildBoardEvent>();
	private final TileBlockFactory tileBlockFactory;
	private final Hangar hangar;
	private int numTotalShips;

	// NOTE: Does not check if all ships can fit inside the board. (It always does on 10x10 board with default ships though)
	public BuildBoard(Hangar hangar, BoardSize boardSize) {
		super(boardSize);
		tileBlockFactory = new TileBlockFactory(tiles, boardSize.getNumColumns());
		this.hangar = hangar;
		this.numTotalShips = hangar.getNumCurrentShips();
	}
	
	public void addBuildBoardEvent(BuildBoardEvent event) {
		buildBoardEvents.add(event);
	}
	public void placeShip(Ship ship, Point tileCoords, Orientation orientation) {
		if (!canPlace(ship, tileCoords, orientation)) return;
		TileBlock tileBlock = tileBlockFactory.createTileBlock(tileCoords, ship.getSize(), orientation);
		tileBlock.setTileTypeAll(TileType.SHIP);
		ArrayList<TileBlock> previousBlocks = tileBlockMap.getOrDefault(ship, new ArrayList<TileBlock>());
		previousBlocks.add(tileBlock);
		tileBlockMap.put(ship, previousBlocks);
		hangar.subtract(ship);
		
		Point range = tileBlock.getTileRange();
		String coordsRangeString = Utility.toCoordsRange(tileCoords, range);
		BattleshipsLogger.logger.info("Placed '" + ship.name() + "' " + coordsRangeString);
		
		boolean hangarEmpty = hangar.isEmpty();
		for (BuildBoardEvent event : buildBoardEvents) {
			event.onShipPlaced(ship, tileBlock);
			if (hangarEmpty) {
				event.onAllShipsPlaced();
			}
		}
	}
	
	public Hangar getHangar() {
		return hangar;
	}
	public int getCurrentShipAmount(Ship ship) {
		return hangar.getShipAmount(ship);
	}
	public int getNumTotalShips() {
		return numTotalShips;
	}
	
	public boolean canPlace(Ship ship, Point tileCoords, Orientation orientation) {
		int shipSize = ship.getSize();
		if (!(tileBlockFactory.canCreateBlock(tileCoords, shipSize, orientation))) return false;
		TileBlock checkBlock = tileBlockFactory.createTileBlock(tileCoords, shipSize, orientation);
		if (!checkBlock.isInBounds(Point.zero(), boardSize.getDimensions().sub(1, 1))) return false;
		for (ArrayList<TileBlock> blocks : tileBlockMap.values()) {
			for (TileBlock block : blocks) {
				if (block.collidesWith(checkBlock)) return false;
			}			
		}
		
		return true;
	}
	
	// TODO: Wrong place?
	// TODO: Change to [Point start, Point end]
	public Point[] getOccupiedSpots(Point start, int length, Orientation orientation) {
		ArrayList<Point> occupiedSpots = new ArrayList<Point>();
		for (int i = 0; i < length; i++) {
			Point coords = Utility.getShiftedPoint(start, i, orientation);
			if (coords.getX() >= boardSize.getNumColumns() || coords.getY() >= boardSize.getNumRows()) continue;
			if (getTileType(coords) != TileType.EMPTY) {
				occupiedSpots.add(coords);
			}
		}
		
		return occupiedSpots.toArray(new Point[occupiedSpots.size()]);
	}
}
