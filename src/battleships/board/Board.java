package battleships.board;

import java.util.ArrayList;
import java.util.HashMap;

import battleships.board.Tile.TileType;
import battleships.hangar.Ship;
import battleships.util.Point;
import battleships.util.Utility;

public abstract class Board {
	public static final class BoardSize {
		public static final int MIN_NUM_ROWS 		= 5;
		public static final int MIN_NUM_COLUMNS 	= 5;
		public static final int DEFAULT_NUM_ROWS 	= 10;
		public static final int DEFAULT_NUM_COLUMNS = 10;
		public static final int TILE_SIZE 			= 32;
		public static final BoardSize DEFAULT 		= new BoardSize(DEFAULT_NUM_ROWS, DEFAULT_NUM_COLUMNS);
		public static final BoardSize JAPANESE 		= new BoardSize(5, 5);
		
		private final int numRows;
		private final int numColumns;
		
		public BoardSize(int numRows, int numColumns) {
			if (numRows < MIN_NUM_ROWS) throw new IllegalArgumentException("Number of rows cannot be less than " + MIN_NUM_ROWS);
			if (numColumns < MIN_NUM_COLUMNS) throw new IllegalArgumentException("Number of columns cannot be less than " + MIN_NUM_COLUMNS);
			this.numRows = numRows;
			this.numColumns = numColumns;
		}
		
		public int getNumRows() {
			return numRows;
		}
		public int getNumColumns() {
			return numColumns;
		}
		public int getNumSquares() {
			return numRows * numColumns;
		}
		public int getWidth() {
			return numColumns * TILE_SIZE;
		}
		public int getHeight() {
			return numRows * TILE_SIZE;
		}
		public Point getDimensions() {
			return new Point(numColumns, numRows);
		}
	}
	
	protected final BoardSize boardSize;
	protected HashMap<Ship, ArrayList<TileBlock>> tileBlockMap = new HashMap<Ship, ArrayList<TileBlock>>();
	protected Tile[] tiles;
	
	public Board(BoardSize boardSize) {
		this.boardSize = boardSize;
		
		int numSquares = boardSize.getNumSquares();
		int numRows = boardSize.getNumRows();
		int numColumns = boardSize.getNumColumns();
		tiles = new Tile[numSquares];
		for (int y = 0; y < numRows; y++) {
			for (int x = 0; x < numColumns; x++) {
				int index = Utility.getArrayIndex(x, y, numColumns);
				tiles[index] = new Tile(new Point(x, y));
			}
		}
	}
	
	public BoardSize getBoardSize() {
		return boardSize;
	}
	public Tile[] getTiles() {
		return tiles;
	}
	public Tile getTile(Point tileCoords) {
		return tiles[Utility.getArrayIndex(tileCoords, boardSize.getNumColumns())];
	}
	public TileType getTileType(Point tileCoords) {
		return getTile(tileCoords).getType();
	}
	public HashMap<Ship, ArrayList<TileBlock>> getTileBlockMap() {
		return tileBlockMap;
	}
}
