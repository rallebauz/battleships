package battleships.board;

import battleships.board.Tile.TileType;
import battleships.util.Orientation;
import battleships.util.Point;
import battleships.util.Utility;

public class TileBlock {
	public static final class TileBlockFactory {
		private final Tile[] factoryTiles;
		private final int arrayWidth;
		private final int arrayHeight;
		
		public TileBlockFactory(Tile[] factoryTiles, int arrayWidth) {
			if (factoryTiles.length % arrayWidth != 0) {
				throw new RuntimeException("Factory tiles do not match proportions that are set.");
			}
			this.factoryTiles = factoryTiles;
			this.arrayWidth = arrayWidth;
			this.arrayHeight = factoryTiles.length / arrayWidth;
		}
		
		public TileBlock createTileBlock(Point startPoint, int length, Orientation orientation) {
			if (!canCreateBlock(startPoint, length, orientation)) throw new IllegalArgumentException("Cannot create block; Size is zero or requested tiles are not part of factory");
			boolean factoryTileExists;
			Tile[] blockTiles = new Tile[length];
			for (int i = 0; i < length; i++) {
				factoryTileExists = false;
				Point currentPoint = Utility.getShiftedPoint(startPoint, i, orientation);
				for (Tile factoryTile : factoryTiles) {
					if (factoryTile.getCoords().equals(currentPoint)) {
						factoryTileExists = true;
						blockTiles[i] = factoryTile;
					}
				}
				if (!factoryTileExists) {
					throw new RuntimeException("Expected tile does not exist in factory.");
				}
			}
			TileBlock tileBlock = new TileBlock(blockTiles, orientation);
			return tileBlock;
		}
		public boolean canCreateBlock(Point startPoint, int size, Orientation orientation) {
			Point endPoint = Utility.getShiftedPoint(startPoint, size, orientation);
			if (size <= 0) return false;
			return !((startPoint.getX() < 0 || endPoint.getX() >= arrayWidth) ||
					(startPoint.getY() < 0 || endPoint.getY() >= arrayHeight));
		}
	}
	
	private final Tile[] tiles;
	private final Orientation orientation;
	
	private TileBlock(Tile[] tiles, Orientation orientation) {
		this.tiles = tiles;
		this.orientation = orientation;
	}
	
	public void setTileTypeAll(TileType type) {
		for (Tile tile : tiles) {
			tile.setType(type);
		}
	}
	
	public Tile[] getTiles() {
		return tiles;
	}
	public Orientation getOrientation() {
		return orientation;
	}
	public Point getStartCoords() {
		return tiles[0].getCoords();
	}
	public Point getEndCoords() {
		return tiles[tiles.length - 1].getCoords();
	}
	public Point getTileRange() {
		return (Utility.getShiftedPoint(Point.zero(), tiles.length - 1, orientation));
	}
	public int getNumTiles() {
		return tiles.length;
	}
	
	public boolean isUnified(TileType type) {
		for (Tile tile : tiles) {
			if (tile.getType() != type) return false;
		}
		return true;
	}
	public boolean isInBounds(Point begin, Point end) {
		Point blockStart = getStartCoords();
		Point blockEnd = getEndCoords();
		return !((blockStart.getX() < begin.getX()) || (blockEnd.getX() > end.getX()) ||
				 (blockStart.getY() < begin.getY()) || (blockEnd.getY() > end.getY()));
	}
	// TODO: Use isInBounds somehow
	public boolean collidesWith(TileBlock other) {
		Point blockStart = getStartCoords();
		Point blockEnd = getEndCoords();
		Point otherStart = other.getStartCoords();
		Point otherEnd = other.getEndCoords();
		return ((blockStart.getX() < otherEnd.getX() + 1) && (blockEnd.getX() + 1 > otherStart.getX()) &&
				(blockStart.getY() < otherEnd.getY() + 1) && (blockEnd.getY() + 1 > otherStart.getY()));
	}
}
