package battleships.board;

import battleships.util.Point;

public class Tile {
	public enum TileType {
		EMPTY,
		SHIP,
		WRECKAGE
	}
	
	private final Point coords;
	private TileType type = TileType.EMPTY;
	private boolean hidden = true;
	
	public Tile(Point coords) {
		this.coords = coords;
	}
	
	public boolean attack() {
		boolean hitShip = (type == TileType.SHIP);
		if (hitShip) {
			type = TileType.WRECKAGE;
		}
		show();
		return hitShip;
	}
	public void setType(TileType type) {
		this.type = type;
	}
	public void show() {
		this.hidden = false;
	}
	
	public Point getCoords() {
		return coords;
	}
	public TileType getType() {
		return type;
	}
	public boolean isHidden() {
		return hidden;
	}
}
