package battleships.board;

import java.util.ArrayList;
import java.util.List;

import battleships.board.Tile.TileType;
import battleships.util.BattleshipsLogger;
import battleships.util.Point;

public class FightBoard extends Board {
	public static final class AttackStrategy {
		private AttackStrategy() {}
		
		public static void attackRandomHidden(FightBoard targetBoard) {
			Tile[] tiles = targetBoard.tiles;
			List<Point> hiddenTileIndeces = new ArrayList<Point>();
			for (int i = 0; i < tiles.length; i++) {
				if (tiles[i].isHidden()) {
					hiddenTileIndeces.add(tiles[i].getCoords());
				}
			}
			if (hiddenTileIndeces.size() <= 0) throw new RuntimeException("Could not attack; No attackable tiles remaining");
			int index = (int)(Math.random() * hiddenTileIndeces.size());
			Point attackPoint = hiddenTileIndeces.get(index);
			targetBoard.attack(attackPoint);
		}
	}
	public interface FightBoardEvent {
		public void onAttack(AttackResult result, Point tileCoords);
		public void onAllShipsDestroyed();
	}
	public enum AttackResult {
		MISS,
		HIT;
	}
	
	private final ArrayList<FightBoardEvent> fightBoardEvents = new ArrayList<FightBoardEvent>();
	
	public FightBoard(BuildBoard buildBoard) {
		super(buildBoard.getBoardSize());
		
		this.tiles = buildBoard.tiles;
		this.tileBlockMap = buildBoard.tileBlockMap;
	}
	
	public void attack(Point tileCoords) {
		if (!canTargetTile(tileCoords)) return;
		
		boolean hitShip = getTile(tileCoords).attack();
		AttackResult attackResult = (hitShip ? AttackResult.HIT : AttackResult.MISS);

		BattleshipsLogger.logger.info(attackResult.name() + " " + tileCoords.toString());
		
		boolean allDestroyed = allShipsDestroyed();
		for (FightBoardEvent event : fightBoardEvents) {
			event.onAttack(attackResult, tileCoords);
			if (allDestroyed) {
				event.onAllShipsDestroyed();
			}
		}
	}
	
	public void addFightBoardEvent(FightBoardEvent event) {
		fightBoardEvents.add(event);
	}

	public boolean canTargetTile(Point tileCoords) {
		return getTile(tileCoords).isHidden();
	}
	public boolean allShipsDestroyed() {
		for (ArrayList<TileBlock> blocks : tileBlockMap.values()) {
			for (TileBlock block : blocks) {
				if (!block.isUnified(TileType.WRECKAGE)) return false;
			}
		}
		return true;
	}
}
