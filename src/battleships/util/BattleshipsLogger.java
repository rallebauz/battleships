package battleships.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public final class BattleshipsLogger {
	public static final String LOGS_PATH_FOLDER = "logs";
	public static final String LOGGER_NAME = "BattleshipsLogger";
	public static final Logger logger;
	
	static {
		logger = Logger.getLogger(LOGGER_NAME);
		/*	REMOVE COMMENT TO ENABLE LOGGER (patentent solutiun dont copy)
		try {
			new File(LOGS_PATH_FOLDER).mkdir();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH.mm");
			String logFilePath = dateFormat.format(new Date());
			FileHandler fileHandler = new FileHandler(LOGS_PATH_FOLDER + "/" + logFilePath + ".log");
			logger.addHandler(fileHandler);
			SimpleFormatter loggingFormatter = new SimpleFormatter();
			fileHandler.setFormatter(loggingFormatter);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
		*/
	}
	
	private BattleshipsLogger() {}
}
