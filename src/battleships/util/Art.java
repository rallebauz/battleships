package battleships.util;

import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public final class Art {
	public static final Image SPRITE_SHEET 		= loadImage("sprite_sheet.png");
	public static final Image SUBMARINE 		= spliceImage(SPRITE_SHEET, 0, 0, 32, 32);
	public static final Image DESTROYER 		= spliceImage(SPRITE_SHEET, 0, 32, 64, 32);
	public static final Image CRUISER 			= spliceImage(SPRITE_SHEET, 0, 64, 96, 32);
	public static final Image BATTLESHIP 		= spliceImage(SPRITE_SHEET, 0, 96, 128, 32);
	public static final Image AIRCRAFT_CARRIER 	= spliceImage(SPRITE_SHEET, 0, 128, 160, 32);
	public static final Image WATER 			= spliceImage(SPRITE_SHEET, 64, 32, 32, 32);
	public static final Image WATER_DARK 		= spliceImage(SPRITE_SHEET, 96, 32, 32, 32);
	public static final Image HIT 				= spliceImage(SPRITE_SHEET, 32, 0, 32, 32);
	public static final Image MISS 				= spliceImage(SPRITE_SHEET, 64, 0, 32, 32);
	public static final Image INVALID 			= spliceImage(SPRITE_SHEET, 96, 0, 32, 32);
	public static final Image GRID 				= spliceImage(SPRITE_SHEET, 96, 64, 32, 32);
	
	private Art() {}
	
	public static Image loadImage(String path) {
		try {
			return new Image(path);
		} catch (NullPointerException | IllegalArgumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	/*
	public static Image loadImage(String path, int clipStartX, int clipStartY, int clipWidth, int clipHeight) {
		Image base = loadImage(path);
		WritableImage writableImage = new WritableImage(clipWidth, clipHeight);
		PixelWriter writablePixelWriter = writableImage.getPixelWriter();
		writablePixelWriter.setPixels(0, 0, clipWidth, clipHeight, base.getPixelReader(), clipStartX, clipStartY);
		return writableImage;
	}
	*/
	public static Image spliceImage(Image base, int clipStartX, int clipStartY, int clipWidth, int clipHeight) {
		WritableImage writableImage = new WritableImage(clipWidth, clipHeight);
		PixelWriter writablePixelWriter = writableImage.getPixelWriter();
		writablePixelWriter.setPixels(0, 0, clipWidth, clipHeight, base.getPixelReader(), clipStartX, clipStartY);
		return writableImage;
	}
}
