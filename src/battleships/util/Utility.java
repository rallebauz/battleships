package battleships.util;

public final class Utility {
	private Utility() {}
	
	public static int clamp(int val, int min, int max) {
		return Math.max(min, Math.min(max, val));
	}
	public static String toCoordsRange(Point start, Point range) {
		return toCoordsRange(start.getX(), start.getY(), range.getX(), range.getY());
	}
	public static String toCoordsRange(int x, int y, int w, int h) {
		return "(" + x + (w!=0?("->"+(x+w)):"") + ", " + y + (h!=0?("->"+(y+h)):"") + ")";
	}
	public static Point getRandomPoint(int xStart, int yStart, int xEnd, int yEnd) {
		return new Point((int)(xStart + Math.random() * (xEnd - xStart)), (int)(yStart + Math.random() * (yEnd - yStart)));
	}
	public static Point getRandomPointFromZero(int xr, int yr) {
		return getRandomPoint(0, 0, xr, yr);
	}
	public static Point getShiftedPoint(Point startPoint, int shift, Orientation orientation) {
		boolean hor = orientation.isHorizontal();
		Point ret = new Point(startPoint.getX() + (hor ? shift : 0), startPoint.getY() + (hor ? 0 : shift));
		return ret;
	}
	public static int getArrayIndex(Point point, int arrayWidth) {
		return (point.getX() + point.getY() * arrayWidth);
	}
	public static int getArrayIndex(int x, int y, int arrayWidth) {
		return (x + y * arrayWidth);
	}
}
