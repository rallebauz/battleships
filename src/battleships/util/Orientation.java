package battleships.util;

public enum Orientation {
	HORIZONTAL,
	VERTICAL;
	public boolean isHorizontal() {
		return (this.equals(HORIZONTAL));
	}
	public boolean isVertical() {
		return (this.equals(VERTICAL));
	}
	public static Orientation getPerpendicular(Orientation orientation) {
		return (orientation == HORIZONTAL ? VERTICAL : HORIZONTAL);
	}
}