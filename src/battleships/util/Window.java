package battleships.util;

import javafx.stage.Stage;

public final class Window {
	public static final int WIDTH 			= 800;
	public static final int HEIGHT 			= 600;
	public static final String TITLE 		= "Battleships";
	public static final boolean RESIZABLE 	= false;
	
	private Window() {}
	
	public static void initializeStage(Stage stage) {
		stage.setWidth(Window.WIDTH);
		stage.setHeight(Window.HEIGHT);
		stage.setTitle(Window.TITLE);
		stage.setResizable(Window.RESIZABLE);
		stage.show();
	}
}
