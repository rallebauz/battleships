package battleships.scene;


import battleships.GameState;
import battleships.board.FightBoard;
import battleships.board.FightBoard.AttackResult;
import battleships.board.FightBoard.AttackStrategy;
import battleships.board.FightBoard.FightBoardEvent;
import battleships.scene.layout.pane.BattleLogPane;
import battleships.scene.layout.pane.FightBoardPane;
import battleships.scene.layout.pane.InputTileLayer.TileEvent;
import battleships.util.BattleshipsLogger;
import battleships.util.Point;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

// TODO: Flash newly acted upon square so user sees what got hit.
// FIXME: Return to starting scene after game has ended.
public class BattleFightScene extends BattleshipScene<BorderPane> {
	public FightBoardPane friendlyBoardPane;
	public FightBoardPane enemyBoardPane;
	private FightBoard friendlyBoard;
	private FightBoard enemyBoard;
	public final BattleLogPane battleLogPane = new BattleLogPane();
	private boolean playerTurn = true;

	public BattleFightScene(GameState gameState) {
		super(new BorderPane());
		
		root.setRight(battleLogPane);
	}

	// FIXME: Keep track of who's turn it is
	@Override
	public void onSceneStart(GameState gameState) {
		friendlyBoard = gameState.getActiveFightBoard(GameState.BOARD_FRIENDLY);
		friendlyBoardPane = new FightBoardPane(friendlyBoard);
		enemyBoard = gameState.getActiveFightBoard(GameState.BOARD_ENEMY);
		enemyBoardPane = new FightBoardPane(enemyBoard);
		enemyBoardPane.hideShips();
		
//		friendlyBoard.setCanAttack(!playerTurn);
//		enemyBoard.setCanAttack(playerTurn);
		if (!playerTurn) {
			aiAttack();
		}
		friendlyBoard.addFightBoardEvent(new FightBoardEvent() {
			@Override
			public void onAttack(AttackResult result, Point tileCoords) {
				handleWhenEnemyAttacked(result, tileCoords);
			}
			@Override
			public void onAllShipsDestroyed() {
				endGame();
				battleLogPane.addEntry("(Restart to play again)", Color.DARKGRAY);
				battleLogPane.addEntry("No friendly ships left", Color.RED);
				battleLogPane.addEntry("You lost", Color.RED);
				BattleshipsLogger.logger.info("Player lost");
			}
		});
		enemyBoard.addFightBoardEvent(new FightBoardEvent() {
			@Override
			public void onAttack(AttackResult result, Point tileCoords) {
				handleWhenPlayerAttacked(result, tileCoords);
				
				aiAttack();
			}
			@Override
			public void onAllShipsDestroyed() {
				endGame();
				battleLogPane.addEntry("(Restart to play again)", Color.DARKGRAY);
				battleLogPane.addEntry("All enemies destroyed", Color.BLUE);
				battleLogPane.addEntry("You won!", Color.BLUE);
				BattleshipsLogger.logger.info("Player won");
			}
		});
		enemyBoardPane.waterTiles.addTileEvent(new TileEvent() {
			@Override
			public void tileClicked(Point tileCoords) {
				enemyBoardPane.getBoard().attack(tileCoords);
			}
			@Override
			public void tileEntered(Point tileCoords) {
				root.setCursor(Cursor.CROSSHAIR);
			}
			@Override
			public void tileExited(Point tileCoords) {
				root.setCursor(Cursor.DEFAULT);
			}
		});

		VBox friendlyBox = new VBox();
		friendlyBox.setAlignment(Pos.TOP_CENTER);
		Label friendlyWaters = new Label("Friendly waters");
		friendlyWaters.setFont(Font.font(16));
		friendlyBox.getChildren().addAll(friendlyWaters, friendlyBoardPane);
		friendlyBox.setScaleX(0.75);
		friendlyBox.setScaleY(0.75);
		
		VBox enemyBox = new VBox();
		enemyBox.setAlignment(Pos.TOP_CENTER);
		Label enemyWaters = new Label("Enemy waters");
		enemyWaters.setFont(Font.font(16));
		enemyBox.getChildren().addAll(enemyWaters, enemyBoardPane);
		
		root.setLeft(friendlyBox);
		root.setCenter(enemyBox);
	}
	@Override
	public void onSceneEnd(GameState gameState) {}
	
	private void aiAttack() {
		if (playerTurn) return;
		AttackStrategy.attackRandomHidden(friendlyBoard);
		playerTurn = true;
	}
	
	private void handleWhenEnemyAttacked(AttackResult result, Point tileCoords) {
		handleWhenAnyAttacked(result, tileCoords);
		if (result == AttackResult.HIT) {
			battleLogPane.addEntry("Got hit " + tileCoords, Color.RED);
		}
	}
	private void handleWhenPlayerAttacked(AttackResult result, Point tileCoords) {
		handleWhenAnyAttacked(result, tileCoords);
		if (result == AttackResult.HIT) {
			battleLogPane.addEntry("You hit " + tileCoords, Color.BLUE);
		}
	}
	private void handleWhenAnyAttacked(AttackResult result, Point tileCoords) {
		playerTurn = !playerTurn;
//		friendlyBoardPane.getBoard().setCanAttack(!playerTurn);
//		enemyBoardPane.getBoard().setCanAttack(playerTurn);
	}
	
	private void endGame() {
		friendlyBoardPane.setDisable(true);
		enemyBoardPane.setDisable(true);
		enemyBoardPane.showShips();
	}
}
