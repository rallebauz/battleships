package battleships.scene;

import battleships.GameState;
import javafx.scene.layout.BorderPane;

public class MainMenuScene extends BattleshipScene<BorderPane> {
	public MainMenuScene(GameState gameState) {
		super(new BorderPane());
	}
	
	@Override
	public void onSceneStart(GameState gameState) {}
	@Override
	public void onSceneEnd(GameState gameState) {}
}
