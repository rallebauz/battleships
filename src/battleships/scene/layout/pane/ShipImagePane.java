package battleships.scene.layout.pane;

import battleships.board.Board.BoardSize;
import battleships.board.TileBlock;
import battleships.hangar.Ship;
import battleships.util.Orientation;
import battleships.util.Point;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class ShipImagePane extends Pane {
	private final ImageView imageView = new ImageView();
	private Ship ship = null;
	
	public ShipImagePane() {
		imageView.setDisable(true);
		
		this.getChildren().add(imageView);
		this.setDisable(true);
	}
	
	public void setFromTileBlock(TileBlock tileBlock) {
		this.setPosAndRot(tileBlock.getStartCoords(), tileBlock.getOrientation());		
	}
	public void setShip(Ship ship) {
		if (this.ship == ship) return;
		this.ship = ship;
		int paneWidth = ship.getSize() * BoardSize.TILE_SIZE;
		int paneHeight = BoardSize.TILE_SIZE;
		this.setMinWidth(paneWidth);
		this.setPrefWidth(paneWidth);
		this.setMaxWidth(paneWidth);
		this.setMinHeight(paneHeight);
		this.setPrefHeight(paneHeight);
		this.setMaxHeight(paneHeight);
		imageView.setImage(ship.getImage());
		imageView.setFitWidth(paneWidth);
		imageView.setFitHeight(paneHeight);
	}
	public void setPosition(Point tileCoords, Orientation orientation) {
		this.setPosAndRot(tileCoords, orientation);
	}
	
	public ShipImagePane copy() {
		ShipImagePane ret = new ShipImagePane();
		ret.setShip(this.ship);
		ret.setRotate(this.getRotate());
		ret.setLayoutX(this.getLayoutX());
		ret.setLayoutY(this.getLayoutY());
		return ret;
	}
	
	private void setPosAndRot(Point tileCoords, Orientation orientation) {
		boolean horizontal = orientation.isHorizontal();
		this.setRotate(horizontal ? 0 : 270);
		if (orientation.isHorizontal()) {
			this.setLayoutX(tileCoords.getX() * BoardSize.TILE_SIZE);
			this.setLayoutY(tileCoords.getY() * BoardSize.TILE_SIZE);
		} else {
			int shipSize = ship.getSize();
			int squaresOnSmallSide = Math.floorDiv(shipSize, 2);
			double offset = (shipSize%2==0?(shipSize-squaresOnSmallSide-0.5):squaresOnSmallSide);
			this.setLayoutX(((double)tileCoords.getX() - offset) * BoardSize.TILE_SIZE);
			this.setLayoutY(((double)tileCoords.getY() + offset) * BoardSize.TILE_SIZE);
		}
	}
}
