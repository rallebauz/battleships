package battleships.scene.layout.pane;

import java.util.ArrayList;
import java.util.Map;

import battleships.board.Board;
import battleships.board.Tile;
import battleships.board.TileBlock;
import battleships.hangar.Ship;
import battleships.scene.layout.pane.InputTileLayer.TileEvent;
import battleships.util.Art;
import battleships.util.Point;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public abstract class BoardPane<T extends Board> extends BorderPane {
	protected final T board;
	public final WaterTileLayer waterTiles;
	protected final DecorTileLayer decorTiles;
	protected final TileLayer gridLines;
	protected final ShipDisplayPane shipDisplay;
	protected final Group tileGroup;
	
	public BoardPane(T board) {
		this.board = board;
		waterTiles = new WaterTileLayer(board);
		decorTiles = new DecorTileLayer(board);
		gridLines = new TileLayer(board);
		for (int y = 0; y < board.getBoardSize().getNumRows(); y++) {
			for (int x = 0; x < board.getBoardSize().getNumColumns(); x++) {
				gridLines.getTile(new Point(x, y)).setImage(Art.GRID);
			}
		}
		shipDisplay = new ShipDisplayPane();
		
		// Place ships onto pane based on existing ships on board.
		for (Map.Entry<Ship, ArrayList<TileBlock>> entry : board.getTileBlockMap().entrySet()) {
			for (TileBlock block : entry.getValue()) {
				placeShip(entry.getKey(), block);
			}
		}
		
		Label coordinates = new Label("(0, 0)");
		waterTiles.addTileEvent(new TileEvent() {
			@Override
			public void tileClicked(Point tileCoords) {}
			@Override
			public void tileEntered(Point tileCoords) {
				coordinates.setText(tileCoords.toString());
			}
			@Override
			public void tileExited(Point tileCoords) {}
		});
		tileGroup = new Group();
		tileGroup.getChildren().add(waterTiles);
		tileGroup.getChildren().add(gridLines);
		tileGroup.getChildren().add(shipDisplay);
		tileGroup.getChildren().add(decorTiles);
		VBox boardBox = new VBox();
		boardBox.getChildren().addAll(tileGroup, coordinates);
		this.setCenter(boardBox);
	}
	
	public void hideShips() {
		shipDisplay.setVisible(false);
		waterTiles.reset();
	}
	public void showShips() {
		shipDisplay.setVisible(true);
	}
	
	public T getBoard() {
		return board;
	}
	
	protected void placeShip(Ship ship, TileBlock tileBlock) {
		Tile[] tiles = tileBlock.getTiles();
		for (Tile tile : tiles) {
			waterTiles.darken(tile.getCoords());
		}
		shipDisplay.addShip(ship, tileBlock);
	}
}
