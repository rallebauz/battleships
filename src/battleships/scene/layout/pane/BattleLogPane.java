package battleships.scene.layout.pane;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class BattleLogPane extends ScrollPane {
	private static final String BATTLE_LOG_CSS_CLASS = "battle-log";
	private final VBox entries;
	
	public BattleLogPane() {
		VBox content = new VBox();
		Label label = new Label("Combat log");
		label.setTextFill(Color.DARKGRAY);
		label.setAlignment(Pos.TOP_CENTER);
		entries = new VBox();
		content.getChildren().addAll(label, entries);
		this.setHbarPolicy(ScrollBarPolicy.NEVER);
		this.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		this.setContent(content);
		
		this.getStyleClass().add(BATTLE_LOG_CSS_CLASS);
		int w = 400;
		this.setMinWidth(w);
		this.setPrefWidth(w);
		this.setMaxWidth(w);
		int h = 400;
		this.setMinHeight(h);
		this.setPrefHeight(h);
		this.setMaxHeight(h);
	}
	
	public void addEntry(String text, Color color) {
		Text entry = new Text(text);
		entry.setFill(color);
		entry.setStroke(Color.BLACK);
		entry.setStrokeWidth(0.1);
		entries.getChildren().add(0, entry);
	}
	
}
