package battleships.scene.layout.pane;

import battleships.board.Board;
import battleships.board.Board.BoardSize;
import battleships.util.Point;
import javafx.scene.Group;
import javafx.scene.image.ImageView;

public class TileLayer extends Group {
	protected final BoardSize boardSize;
	protected final ImageView[] tiles;
	
	public TileLayer(Board board) {
		this.boardSize = board.getBoardSize();
		int numRows = boardSize.getNumRows();
		int numColumns = boardSize.getNumColumns();
		int tileSize = BoardSize.TILE_SIZE;
		tiles = new ImageView[boardSize.getNumSquares()];
		for (int y = 0; y < numRows; y++) {
			for (int x = 0; x < numColumns; x++) {
				ImageView tile = new ImageView();
				tile.setFitWidth(tileSize);
				tile.setFitHeight(tileSize);
				tile.setLayoutX(x * tileSize);
				tile.setLayoutY(y * tileSize);
				
				tiles[x + y * numColumns] = tile;
				this.getChildren().add(tile);
			}
		}
		this.setDisable(true);
	}
	public void erase(Point tileCoords) {
		getTile(tileCoords).setImage(null);
	}
	
	protected ImageView getTile(Point tileCoords) {
		return tiles[tileCoords.getX() + tileCoords.getY() * boardSize.getNumColumns()];
	}
}
