package battleships.scene.layout.pane;

import battleships.board.Board;
import battleships.util.Art;
import battleships.util.Point;

public class DecorTileLayer extends TileLayer {
	public DecorTileLayer(Board board) {
		super(board);
		//this.setDisable(true);
	}
	
	public void miss(Point tileCoords) {
		getTile(tileCoords).setImage(Art.MISS);
	}
	public void hit(Point tileCoords) {
		getTile(tileCoords).setImage(Art.HIT);
	}
	public void invalid(Point tileCoords) {
		getTile(tileCoords).setImage(Art.INVALID);
	}
}
