package battleships.scene.layout.pane;

import battleships.board.BuildBoard;
import battleships.hangar.Hangar;
import battleships.hangar.Ship;
import battleships.scene.layout.pane.InputTileLayer.TileEvent;
import battleships.scene.layout.pane.ShipPlacementPane.ShipPlacementEvent;
import battleships.scene.layout.pane.ShipSelectionPane.ShipSelectEvent;
import battleships.util.Orientation;
import battleships.util.Point;

public class PlayerBuildBoardPane extends BuildBoardPane {
	private final ShipPlacementPane shipPlacement;
	private final ShipSelectionPane shipSelection;
	
	public PlayerBuildBoardPane(BuildBoard board) {
		super(board);
		
		shipPlacement = new ShipPlacementPane(this);
		shipPlacement.setMarkerShip(board.getHangar().getFirstAvailableShip());
		shipPlacement.addShipPlacementEvent(new ShipPlacementEvent() {
			@Override
			public void onTileClicked(Ship ship, Point tileCoords, Orientation orientation) {
				if (!board.canPlace(ship, tileCoords, orientation)) return;
				
				board.placeShip(ship, tileCoords, orientation);
				
				Hangar hangar = board.getHangar();
				if (hangar.isEmpty()) {
					shipPlacement.setVisible(false);
				} else {
					if (hangar.hasNo(ship)) {
						shipPlacement.setMarkerShip(hangar.getFirstAvailableShipFrom(ship));
					}
				}
				
				shipSelection.updateSelection();
			}
		});
		
		shipSelection = new ShipSelectionPane(board);
		shipSelection.addShipSelectEvent(new ShipSelectEvent() {
			@Override
			public void shipSelected(Ship ship) {
				shipPlacement.setMarkerShip(ship);
			}
		});
		
		waterTiles.addTileEvent(new TileEvent() {
			@Override
			public void tileClicked(Point tileCoords) {
				shipPlacement.performPlacement(tileCoords);
			}
			@Override
			public void tileEntered(Point tileCoords) {
				shipPlacement.update(tileCoords);
			}
			@Override
			public void tileExited(Point tileCoords) {}
		});
		tileGroup.getChildren().add(shipPlacement);
		this.setRight(shipSelection);
	}
}
