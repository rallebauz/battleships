package battleships.scene.layout.pane;

import java.util.ArrayList;

import battleships.board.TileBlock;
import battleships.hangar.Ship;
import javafx.scene.layout.Pane;

public class ShipDisplayPane extends Pane {
	final ArrayList<ShipImagePane> placedShips = new ArrayList<ShipImagePane>();;
	
	public ShipDisplayPane() {
		this.setDisable(true);
	}
	
	public void addShip(Ship ship, TileBlock tileBlock) {
		ShipImagePane newShipImagePane = new ShipImagePane();
		newShipImagePane.setShip(ship);
		newShipImagePane.setFromTileBlock(tileBlock);
		placedShips.add(newShipImagePane);
		this.getChildren().add(newShipImagePane);
	}
	public void addShipImagePane(ShipImagePane pane) {
		placedShips.add(pane);
		this.getChildren().add(pane);
	}
}
