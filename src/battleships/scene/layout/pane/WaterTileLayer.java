package battleships.scene.layout.pane;

import battleships.board.Board;
import battleships.util.Art;
import battleships.util.Point;
import javafx.scene.image.ImageView;

public class WaterTileLayer extends InputTileLayer {
	public WaterTileLayer(Board board) {
		super(board);
		
		for (ImageView tile : tiles) {
			tile.setImage(Art.WATER);
		}
	}
	
	public void darken(Point tileCoords) {
		getTile(tileCoords).setImage(Art.WATER_DARK);
	}
	
	public void reset() {
		for (ImageView tile : tiles) {
			tile.setImage(Art.WATER);
		}
	}
}
