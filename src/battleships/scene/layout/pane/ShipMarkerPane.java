package battleships.scene.layout.pane;

import battleships.hangar.Ship;
import battleships.util.Orientation;
import battleships.util.Point;
import javafx.scene.layout.Pane;

public class ShipMarkerPane extends Pane {
	
	private final ShipImagePane shipImage;
	
	public ShipMarkerPane() {
		shipImage = new ShipImagePane();
		this.getChildren().add(shipImage);
		this.setDisable(true);
	}
	
	public void update(Ship ship, Point tileCoords, Orientation orientation) {
		shipImage.setShip(ship);
		shipImage.setPosition(tileCoords, orientation);
	}
}
