package battleships.scene.layout.pane;


import java.util.ArrayList;

import battleships.hangar.Ship;
import battleships.util.Orientation;
import battleships.util.Point;
import battleships.util.Utility;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;

public class ShipPlacementPane extends Pane {
	public interface ShipPlacementEvent {
		public void onTileClicked(Ship ship, Point tileCoords, Orientation orientation);
	}
	
	private final ArrayList<ShipPlacementEvent> shipPlacementEvents = new ArrayList<ShipPlacementEvent>();
	private final BuildBoardPane boardPane;
	private final ShipMarkerPane shipMarker;
	private Ship selectedShip;
	private Point lastMarkerPos = Point.zero();
	private Point[] lastInvalidSpots = null;
	private Orientation orientation = Orientation.HORIZONTAL;
	
	public ShipPlacementPane(BuildBoardPane boardPane) {
		this.boardPane = boardPane;
		this.selectedShip = boardPane.board.getHangar().getFirstAvailableShip();
		
		shipMarker = new ShipMarkerPane();
		
		this.getChildren().add(shipMarker);
		
		// TODO: Make rotate key bindable.
		// Rotate marker.
		boardPane.addEventHandler(KeyEvent.KEY_PRESSED, (key)->{
			if (key.getCode() == KeyCode.R) {
				orientation = Orientation.getPerpendicular(orientation);
				updateWithLastPos();
			}
		});
		
		this.setDisable(true);
	}
	
	public void update(Point tileCoords) {
		if (!this.isVisible()) return;
		
		lastMarkerPos = tileCoords;
		
		Point center = centerSquare(tileCoords);
		
		if (lastInvalidSpots != null) {
			for (Point spot : lastInvalidSpots) {
				boardPane.decorTiles.erase(spot);
			}
			lastInvalidSpots = null;
		}
		if (!boardPane.board.canPlace(selectedShip, center, orientation)) {
			Point[] invalidSpots = boardPane.board.getOccupiedSpots(center, selectedShip.getSize(), orientation);
			for (Point spot : invalidSpots) {
				boardPane.decorTiles.invalid(spot);
			}
			lastInvalidSpots = invalidSpots;
		}
		shipMarker.update(selectedShip, center, orientation);
	}
	public void updateWithLastPos() {
		update(lastMarkerPos);
	}
	public void performPlacement(Point tileCoords) {	
		Point center = centerSquare(tileCoords);
		
		if (boardPane.board.canPlace(selectedShip, center, orientation)) {
			for (ShipPlacementEvent event : shipPlacementEvents) {
				event.onTileClicked(selectedShip, center, orientation);
			}
			update(tileCoords);
		}		
	}
	
	public void setMarkerShip(Ship ship) {
		selectedShip = ship;
		updateWithLastPos();
	}
	public void addShipPlacementEvent(ShipPlacementEvent event) {
		shipPlacementEvents.add(event);
	}
	
	private Point centerSquare(Point tileCoords) {
		int shipSize = selectedShip.getSize();
		int x, y;
		if (orientation.isHorizontal()) {
			x = Utility.clamp(tileCoords.getX() - Math.floorDiv(shipSize, 2), 0, boardPane.board.getBoardSize().getNumColumns() - shipSize);
			y = tileCoords.getY();
		} else {
			x = tileCoords.getX();
			y = Utility.clamp(tileCoords.getY() - Math.floorDiv(shipSize, 2), 0, boardPane.board.getBoardSize().getNumRows() - shipSize);
		}
		return new Point(x, y);
	}
}
