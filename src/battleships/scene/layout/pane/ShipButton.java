package battleships.scene.layout.pane;

import battleships.board.Board.BoardSize;
import battleships.hangar.Ship;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

public class ShipButton extends Button {

	private Ship ship;

	public ShipButton(BoardSize boardSize, Ship ship) {
		this.ship = ship;

		int backgroundWidth = BoardSize.TILE_SIZE * ship.getSize();
		int backgroundHeight = BoardSize.TILE_SIZE;
		BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, true);
		BackgroundImage backgroundImage = new BackgroundImage(ship.getImage(), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
		this.setBackground(new Background(backgroundImage));
		this.setMinWidth(backgroundWidth);
		this.setPrefWidth(backgroundWidth);
		this.setMaxWidth(backgroundWidth);
		this.setMinHeight(backgroundHeight);
		this.setPrefHeight(backgroundHeight);
		this.setMaxHeight(backgroundHeight);
	}

	public Ship getShip() {
		return ship;
	}
}
