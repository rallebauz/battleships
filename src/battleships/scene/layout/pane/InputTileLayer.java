package battleships.scene.layout.pane;

import java.util.ArrayList;

import battleships.board.Board;
import battleships.util.Point;
import javafx.scene.image.ImageView;

public class InputTileLayer extends TileLayer {
	public interface TileEvent {
		public void tileClicked(Point tileCoords);
		public void tileEntered(Point tileCoords);
		public void tileExited(Point tileCoords);
	}
	
	private final ArrayList<TileEvent> tileEvents = new ArrayList<TileEvent>();
	
	public InputTileLayer(Board board) {
		super(board);
		
		int numRows = boardSize.getNumRows();
		int numColumns = boardSize.getNumColumns();
		for (int y = 0; y < numRows; y++) {
			for (int x = 0; x < numColumns; x++) {
				final Point coords = new Point(x, y);
				ImageView tile = tiles[x + y * numColumns];
				tile.setOnMouseClicked(e->{
					for (TileEvent event : tileEvents) {
						event.tileClicked(coords);
					}
				});
				tile.setOnMouseEntered(e->{
					for (TileEvent event : tileEvents) {
						event.tileEntered(coords);
					}
				});
				tile.setOnMouseExited(e->{
					for (TileEvent event : tileEvents) {
						event.tileExited(coords);
					}
				});
			}
		}
		this.setDisable(false);
	}
	
	public void addTileEvent(TileEvent event) {
		tileEvents.add(event);
	}
}
