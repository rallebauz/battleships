package battleships.scene.layout.pane;

import battleships.board.BuildBoard;
import battleships.board.BuildBoard.BuildBoardEvent;
import battleships.board.TileBlock;
import battleships.hangar.Ship;

public class BuildBoardPane extends BoardPane<BuildBoard> {
	public BuildBoardPane(BuildBoard board) {
		super(board);
		
		board.addBuildBoardEvent(new BuildBoardEvent() {
			@Override
			public void onShipPlaced(Ship ship, TileBlock tileBlock) {
				placeShip(ship, tileBlock);
			}
			@Override
			public void onAllShipsPlaced() {}
		});
	}
}
