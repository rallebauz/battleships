package battleships.scene.layout.pane;

import java.util.ArrayList;
import java.util.Map;

import battleships.board.BuildBoard;
import battleships.hangar.Hangar;
import battleships.hangar.Ship;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;

public class ShipSelectionPane extends VBox {
	public interface ShipSelectEvent {
		public void shipSelected(Ship ship);
	}
	
	private static final String SELECTED_SHIP_CSS_CLASS = "ship-selected";
	private final ArrayList<ShipSelectEvent> shipSelectEvents = new ArrayList<ShipSelectEvent>();
	private final Hangar hangar;
	private final ShipButton[] shipButtons;
	private int selectedIndex = 0;
	
	public ShipSelectionPane(BuildBoard board) {
		this.hangar = board.getHangar();
		this.setAlignment(Pos.TOP_LEFT);
		
		shipButtons = new ShipButton[Ship.NUM_SHIPS];
		int buttonIndex = 0;
		for (Ship ship : Ship.values()) {
			ShipButton shipButton = new ShipButton(board.getBoardSize(), ship);
			final int bi = buttonIndex;
			shipButton.setOnMouseClicked(e->{
				for (ShipSelectEvent event : shipSelectEvents) {
					event.shipSelected(ship);
				}
				setSelectedIndex(bi);
			});
			shipButtons[buttonIndex++] = shipButton;
			this.getChildren().add(shipButton);
		}
		
		int i = 0;
		for (Map.Entry<Ship, Integer> entry : hangar.getHangarType().getShipAmount().entrySet()) {
			if (entry.getValue() <= 0) {
				shipButtons[i].setDisable(true);
			}
			i++;
		}
		
		setSelectedIndex(selectedIndex);
	}
	
	public void updateSelection() {
		ShipButton shipButton = shipButtons[selectedIndex];
		Ship ship = shipButton.getShip();
		// Check if the selected ship can be placed (if there are any left in the hangar).
		if (hangar.getShipAmount(ship) <= 0) {
			shipButton.getStyleClass().remove(SELECTED_SHIP_CSS_CLASS);
			shipButton.setDisable(true);
			boolean searching = true;
			Ship nextShip = ship.next();
			while (searching) {
				if (hangar.getShipAmount(nextShip) > 0) {
					for (int i = 0; i < shipButtons.length; i++) {
						if (nextShip == shipButtons[i].getShip()) {
							shipButton = shipButtons[i];
							shipButton.getStyleClass().add(SELECTED_SHIP_CSS_CLASS);
							selectedIndex = i;
							searching = false;
							break;
						}
					}
				} else {
					nextShip = nextShip.next();
					if (ship == nextShip) {
						searching = false;
					}
				}
			}
		}
	}
	
	public void setSelectedShip(Ship ship) {
		int i = 0;
		for (ShipButton button : shipButtons) {
			if (button.getShip() == ship) {
				setSelectedIndex(i);
				break;
			}
			i++;
		}
	}
	public void addShipSelectEvent(ShipSelectEvent event) {
		shipSelectEvents.add(event);
	}
	
	public Ship getSelectedShip() {
		return shipButtons[selectedIndex].getShip();
	}
	
	private void setSelectedIndex(int index) {
		shipButtons[selectedIndex].getStyleClass().remove(SELECTED_SHIP_CSS_CLASS);
		selectedIndex = index;
		shipButtons[index].getStyleClass().add(SELECTED_SHIP_CSS_CLASS);
		updateSelection();
	}
}
