package battleships.scene.layout.pane;

import battleships.board.FightBoard;
import battleships.board.FightBoard.AttackResult;
import battleships.board.FightBoard.FightBoardEvent;
import battleships.board.Tile;
import battleships.board.Tile.TileType;
import battleships.util.Point;

public class FightBoardPane extends BoardPane<FightBoard> {
	
	public FightBoardPane(FightBoard board) {
		super(board);
		
		board.addFightBoardEvent(new FightBoardEvent() {
			@Override
			public void onAttack(AttackResult result, Point tileCoords) {
				switch (result) {
				case MISS:
					decorTiles.miss(tileCoords);
					break;
				case HIT:
					decorTiles.hit(tileCoords);
					break;
				}
			}
			@Override
			public void onAllShipsDestroyed() {}
		});
	}

	/*
	public static FightBoardPane fromBuildBoard(BuildBoardPane buildBoardPane) {
		FightBoardPane ret = new FightBoardPane(FightBoard.convertFromBuildBoard(buildBoardPane.board));
		Tile[] oldWaterTiles = buildBoardPane.board.getTiles();
		for (Tile tile : oldWaterTiles) {
			if (tile.getType() != TileType.EMPTY) {
				Point coords = tile.getCoords();
				ret.waterTiles.darken(coords);
			}
		}
		for (ShipImagePane sip : buildBoardPane.shipDisplay.placedShips) {
			ret.shipDisplay.addShipImagePane(sip.copy());
		}
		return ret;
	}
	*/
}
