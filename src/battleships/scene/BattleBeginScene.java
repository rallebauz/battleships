package battleships.scene;

import battleships.GameState;
import battleships.board.Board.BoardSize;
import battleships.board.BuildBoard;
import battleships.board.BuildBoard.BuildBoardEvent;
import battleships.board.BuildBoard.PlacementStrategy;
import battleships.board.FightBoard;
import battleships.board.TileBlock;
import battleships.hangar.Hangar;
import battleships.hangar.Hangar.HangarType;
import battleships.hangar.Ship;
import battleships.scene.layout.pane.BuildBoardPane;
import battleships.scene.layout.pane.FightBoardPane;
import battleships.scene.layout.pane.InputTileLayer.TileEvent;
import battleships.scene.layout.pane.PlayerBuildBoardPane;
import battleships.util.Point;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;


public class BattleBeginScene extends BattleshipScene<BorderPane> {
	public final BuildBoard buildBoard = new BuildBoard(new Hangar(HangarType.CLASSIC), BoardSize.DEFAULT);
	public final PlayerBuildBoardPane buildBoardPane = new PlayerBuildBoardPane(buildBoard);
	public final Button readyButton = new Button("Ready");
	
	public BattleBeginScene(GameState gameState) {
		super(new BorderPane());
		
		VBox topBox = new VBox();
		Label topLabel = new Label("Place ships");
		topLabel.setFont(Font.font(24));
		Label rotateLabel = new Label("[R] to rotate ship");
		rotateLabel.setFont(Font.font(20));
		topBox.setAlignment(Pos.TOP_CENTER);
		topBox.getChildren().addAll(topLabel, rotateLabel);
		
		VBox bottomBox = new VBox();
		bottomBox.setPadding(new Insets(0, 0, 24, 0));
		readyButton.setFont(Font.font(24));
		bottomBox.setAlignment(Pos.TOP_CENTER);
		bottomBox.getChildren().add(readyButton);
		readyButton.setDisable(true);
		
		root.setTop(topBox);
		root.setCenter(buildBoardPane);
		root.setBottom(bottomBox);
		
		buildBoard.addBuildBoardEvent(new BuildBoardEvent() {
			@Override
			public void onShipPlaced(Ship ship, TileBlock tileChain) {}
			@Override
			public void onAllShipsPlaced() {
				buildBoardPane.setDisable(true);
				readyButton.setDisable(false);
			}
		});
		buildBoardPane.waterTiles.addTileEvent(new TileEvent() {
			@Override
			public void tileClicked(Point tileCoords) {}
			@Override
			public void tileEntered(Point tileCoords) {
				root.setCursor(Cursor.HAND);
			}
			@Override
			public void tileExited(Point tileCoords) {
				root.setCursor(Cursor.DEFAULT);
			}
		});
		
		readyButton.setOnAction(e->{
			gameState.changeScene(GameState.SCENE_BATTLE_FIGHT);
		});
	}
	
	@Override
	public void onSceneStart(GameState gameState) {}
	@Override
	public void onSceneEnd(GameState gameState) {
		// vvv TEMP vvv
//		BuildBoard friendBuild = new BuildBoard(new Hangar(HangarType.CLASSIC), BoardSize.DEFAULT);
//		BuildBoardPane friendBuildPane = new BuildBoardPane(friendBuild);
//		friendBuild.placeRemainingShipsRandomly();
		// ^^^ TEMP ^^^
		
		BuildBoard enemyBuildBoard = new BuildBoard(new Hangar(HangarType.CLASSIC), BoardSize.DEFAULT);
//		BuildBoardPane enemyBuildPane = new BuildBoardPane(enemyBuildBoard);
		PlacementStrategy.placeRemainingShipsRandomly(enemyBuildBoard);
		
		
		gameState.generateActiveFightBoard(GameState.BOARD_FRIENDLY, buildBoard);
		gameState.generateActiveFightBoard(GameState.BOARD_ENEMY, enemyBuildBoard);
	}
}

