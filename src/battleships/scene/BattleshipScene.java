package battleships.scene;

import battleships.GameState;
import javafx.scene.Parent;
import javafx.scene.Scene;

public abstract class BattleshipScene<T extends Parent> extends Scene {
	protected final T root;
	
	public BattleshipScene(T root) {
		super(root);
		this.root = root;
		this.getStylesheets().add("battleships.css");
	}
	
	public abstract void onSceneStart(GameState gameState);
	public abstract void onSceneEnd(GameState gameState);
}
