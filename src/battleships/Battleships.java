package battleships;

import battleships.util.BattleshipsLogger;
import battleships.util.Window;
import javafx.application.Application;
import javafx.stage.Stage;

public class Battleships extends Application {
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) {
		BattleshipsLogger.logger.info("Start Battleships");
		
		Window.initializeStage(primaryStage);
		
		new GameState(primaryStage);
	}
}
