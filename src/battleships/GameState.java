package battleships;

import java.util.HashMap;

import battleships.board.BuildBoard;
import battleships.board.FightBoard;
import battleships.scene.BattleBeginScene;
import battleships.scene.BattleFightScene;
import battleships.scene.BattleshipScene;
import battleships.scene.MainMenuScene;
import javafx.stage.Stage;

public class GameState {
	public static final String SCENE_MAIN_MENU 		= "MAIN_MENU";
	public static final String SCENE_BATTLE_BEGIN 	= "BATTLE_BEGIN";
	public static final String SCENE_BATTLE_FIGHT	= "BATTLE_FIGHT";
	
	public static final String BOARD_FRIENDLY	= "FRIENDLY";
	public static final String BOARD_ENEMY 		= "ENEMY";
	
	private final HashMap<String, BattleshipScene<?>> sceneMap = new HashMap<String, BattleshipScene<?>>();
	private final HashMap<String, FightBoard> fightBoardMap = new HashMap<String, FightBoard>();
	private final Stage primaryStage;
	private String currentSceneKey = null;
	
	public GameState(Stage primaryStage) {
		this.primaryStage = primaryStage;
		
		sceneMap.put(SCENE_MAIN_MENU, new MainMenuScene(this));
		sceneMap.put(SCENE_BATTLE_BEGIN, new BattleBeginScene(this));
		sceneMap.put(SCENE_BATTLE_FIGHT, new BattleFightScene(this));
		
		changeScene(SCENE_BATTLE_BEGIN);
	}
	
	// TODO: Scenes do not reset, previous build board is still active. (Separate function for reset and simple change?)
	public void changeScene(String sceneKey) {
		if (!sceneMap.containsKey(sceneKey)) {
			throw new IllegalArgumentException("Invalid scene key '" + sceneKey + "'");
		}
		if (currentSceneKey != null) {
			sceneMap.get(currentSceneKey).onSceneEnd(this);
		}
		currentSceneKey = sceneKey;
		BattleshipScene<?> targetScene = sceneMap.get(sceneKey);
		targetScene.onSceneStart(this);
		primaryStage.setScene(targetScene);
	}
	
	public void generateActiveFightBoard(String boardKey, BuildBoard buildBoard) {
		FightBoard fightBoard = new FightBoard(buildBoard);
		fightBoardMap.put(boardKey, fightBoard);
	}
	
	public FightBoard getActiveFightBoard(String boardKey) {
		if (!fightBoardMap.containsKey(boardKey)) throw new IllegalArgumentException("No active board '" + boardKey + "' exists");
		
		return fightBoardMap.get(boardKey);
	}
}
